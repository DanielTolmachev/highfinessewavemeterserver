ErrorStrings = {-100:"StartingWlmSoftware",
                -101: "Linewidth mode will be enabled",
                -20: "Linewidth too wide",
    -15.0: 'ErrUnitNotAvailable',
 -14.0: 'ErrOutOfRange',
 -13.0: 'ErrDiv0',
 -8.0: 'ErrNoPulse',
 -7.0: 'InfNothingChanged',
 -6.0: 'ErrNotAvailable',
 -5.0: 'ErrWlmMissing',
 -4.0: 'ErrBigSignal',
 -3.0: 'ErrLowSignal',
 -2.0: 'ErrBadSignal',
 -1.0: 'ErrNoSignal',
 0.0: 'ErrNoValue'}

ID_STRING = "HighFiness Wavelength Meter WS/6L TCP server."

class WS_cmd_interpreter(object):
    def __init__(self, device_object,precision = 4):
        super(WS_cmd_interpreter,self).__init__()
        self.ws = device_object
        self.precision = precision
    def process_query(self,query):
        query = query.decode().lower().strip()
        queries = query.split("?")
        responses = []
        for q in queries:
            if q:
                responses.append(self.process_command(q.strip()))
        if not responses:
            response = "Wavemeter Server got unrecognized command:{}".format(query)
        else:
            response = " ".join(responses)
        return response
    def process_command(self,cmd):
        if cmd in ["wl","wavelength"]:
            res = self.ws.getWaveLength()
        elif cmd in ["freq","frequency"]:
            res = self.ws.getFrequency()
        elif cmd in ["lw","linewidth"]:
            res = self.ws.getLineWidthAir()
        elif cmd in ["lwf","linewidthfrequency"]:
            res = self.ws.getLineWidthFreq()
        elif cmd in ["close"]:
            res = self.ws.closeApp()
        elif cmd == "*idn":
            return ID_STRING
        else:
            return "?{}?".format(cmd)
        if res>0:
            return "{:.{}f}".format(res, self.precision)
        elif res <=0:
            if res in ErrorStrings:
                if res==-100:
                    res = "{} Program is launching retry later".format(res, ErrorStrings[res])
                else:
                    res = "{}({})".format(res,ErrorStrings[res])
            else:
                res = "{}".format(res)
            return "{}".format(res)