This is a TCP server for HighFinesse Wavelength Meter Angstrom WS5, WS6, WS7
           
This server should be running on a computer, to witch wavelength meter is connected with USB, thus providing a possibility for querying laser wavelength (frequency) or linewidth through newtork.

Tested with WS6 and WS7 wavemeters

Allows multiple connections

# Platform
win32/win64

# Requirements:
python 3

HighFinesse Wavelength Meter software installed

# Supported commands:
command | description
--------|--------------
*idn?	|Show ID string
wavelength?|
wl?	|Wavelength in vacuum in nm
frequency?|
freq?	|Frequency in THz
linewidth?|
lw?	|Linewidth in nm
linewidthfrequency?|
lwf?	|Linewidth in THz

# Usage comments
Query commands like “wavelength?” return string containing measured value in floating point notation or error code with text explanation.
Multiple queries can be sent in one request

example | explanation
:---------------|------------
< wl? 
> 800.8630 | wavelength in nm
< wl? 
> 0.0(ErrNoValue) | error, probably laser is off
< freq?lwf?
> 374.3368 0.0004| frequency in THz, linewidth in THz
< wl?lw?
> 800.8630 0.0010 | wavelength in nm, linewidth in nm