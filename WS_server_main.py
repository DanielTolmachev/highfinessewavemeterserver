#-------------------------------------------------------------------------------
# Name: 	WS_server_main.pyw
#
# Purpose:  This is a tcp server for HighFinesse Wavelength meter Angstrom WS5-WS7
#           this server should be running on a computer, to witch wavelengthmeter is connected with usb
#           tested with WS6 and WS7 wavemeters
#           allows multiple connections
#
# Created:  Dec 2016 
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"
from __future__ import print_function
from socket import *
import threading,traceback
import ws7_class,ws_cmd_interpreter
BUFF = 1024
# HOST = '127.0.0.1'
HOST = ""
PORT = 50301 



def handler(clientsock,addr,id,wscmd):
    try:
        while True:
            data = clientsock.recv(BUFF)
            if not data: break
            print( repr(addr) + '>> ' + repr(data))
            resp = wscmd.process_query(data)
            clientsock.send(str(resp).encode())
            print("<< ",str(resp).encode())
            # print("[{}] {}  sent:".format(repr(id),repr(addr),repr(resp)))
    except:
        traceback.print_exc()
    finally:
        clientsock.close()
        print (addr, "- closed connection #",id) #log on console

def run_server(address):
    cnt = 0
    serversock = socket(AF_INET, SOCK_STREAM)
    serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    serversock.bind(address)
    serversock.listen(5)
    ws = ws7_class.Wavemeter(autostart=True)
    wscmd = ws_cmd_interpreter.WS_cmd_interpreter(ws)
    while 1:
        print('waiting for connection... listening on port', PORT)
        clientsock, addr = serversock.accept()
        cnt += 1
        print('...connected #', cnt, 'from:', addr)
        th = threading.Thread(target=handler, args=(clientsock, addr, cnt, wscmd))
        th.start()

if __name__=='__main__':
    addr = (HOST, PORT)
    run_server(addr)
