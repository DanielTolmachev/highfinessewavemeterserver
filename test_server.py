from __future__ import print_function
import socket,time
BUFF = 1024
HOST = '127.0.0.1'
PORT = 50301


soc = socket.create_connection((HOST,PORT),1)
def ask(req):
    print("<", req)
    if type(req)==str:
        req = req.encode()
    soc.send(req)
    repl = soc.recv(BUFF)
    print(">", repl.decode())

ask("hello")
ask("*IDN?")
ask("wl?")
ask("lw?")
ask("freq?lwf?")
ask("freq?")

##close socket if you do not plan send commands from console
# soc.close()  
